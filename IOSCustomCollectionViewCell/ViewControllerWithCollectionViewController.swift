    //
    //  Borges Tiles
    //
    //  Created by Fabiana Marotta on 09/12/2018 based on Bifurcan by Devine Lu Linvega
    //  Copyright © 2018 Fabiana Marotta. All rights reserved.
    //
    
    
    
    import UIKit
    import AVFoundation
    import AudioToolbox
    
    class ViewControllerWithCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
        var player: AVAudioPlayer?
        @IBOutlet weak var schermo: UICollectionView!
        let date = Date()
        let calendar = Calendar.current
        var ore_0_9 = 0
        var ore_0_1 = 0
        @IBOutlet weak var ore1: UICollectionView!
        @IBOutlet weak var Ore2: UICollectionView!
        var secondi = 0
        var indice = 0
        var secondi0_5 = 0
        var indice1 = 0
        var minuti2_0_9 = 0
        var minuti1_0_5 = 0
        var indice2 = 0
        var indice3 = 0
        var indice4 = 0
        var indice5 = 0
        
        
        
        let numero0 = [true,true,true,
                       true,false,true,
                       true,false,true,
                       true,false,true,
                       true,true,true]
        
        
        let numero1 = [false,true,false,
                       false,true,false,
                       false,true,false,
                       false,true,false,
                       false,true,false]
        
        
        let numero2 = [true,true,true,
                       false,false,true,
                       true,true,true,
                       true,false,false,
                       true,true,true]
        
        let numero3 = [true,true,true,
                       false,false,true,
                       true,true,true,
                       false,false,true,
                       true,true,true]
        
        
        let numero4 = [true,false,true,
                       true,false,true,
                       true,true,true,
                       false,false,true,
                       false,false,true]
        
        let numero5 = [true,true,true,
                       true,false,false,
                       true,true,true,
                       false,false,true,
                       true,true,true]
        
        
        let numero6 = [true,false,false,
                       true,false,false,
                       true,true,true,
                       true,false,true,
                       true,true,true]
        
        
        let numero7 = [true,true,true,
                       false,false,true,
                       false,false,true,
                       false,false,true,
                       false,false,true]
        
        
        let numero8 = [true,true,true, // otto
            true,false,true,
            true,true,true,
            true,false,true,
            true,true,true]
        
        let numero9 = [true,true,true, // nove
            true,false,true,
            true,true,true,
            false,false,true,
            false,false,true]
        
        @IBOutlet weak var collectionView1: UICollectionView!
        
        @IBOutlet weak var secondi1: UICollectionView!
        @IBOutlet weak var secondi2: UICollectionView!
        @IBOutlet weak var minuti2: UICollectionView!
        @IBOutlet weak var minuti1: UICollectionView!
        var secondo2 = 0
        var secondo3 = 0
        override func viewDidLoad() {
            super.viewDidLoad()
            collectionView1.dataSource = self
            collectionView1.delegate = self
            secondi1.delegate = self
            secondi1.dataSource = self
            secondi2.delegate = self
            secondi2.dataSource = self
            
            minuti2.delegate = self
            minuti2.dataSource = self
            
            minuti1.delegate = self
            minuti1.dataSource = self
            Ore2.delegate = self
            Ore2.dataSource = self
            
            i = 2
            ore1.delegate = self
            ore1.dataSource = self
            let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .light)
            
            let second = calendar.component(.second, from: self.date)
            let minut = calendar.component(.minute, from: self.date)
            let ore = calendar.component(.hour, from: self.date)
            let mc1 = String(minut)
            let mc = String(second)
            let s2 = Array(mc)
            let s3 = Array(mc1)
            
            
            let mc2 = String(ore)
            let s4 = Array(mc2)
            if(s4.count > 1)
            {
                ore_0_9 = Int(String(s4[1]))!
                ore_0_1 = Int(String(s4[0]))!
            }else{
                ore_0_9 = Int(String(s4[0]))!
                ore_0_1 = 0
            }
            
            if(s2.count > 1)
            {
                secondo2 = Int(String(s2[1]))!
                secondo3 = Int(String(s2[0]))!
            }else{
                secondo2 = Int(String(s2[0]))!
                secondo3 = 0
            }
            
            if(s3.count > 1)
            {
                minuti2_0_9 = Int(String(s3[1]))!
                minuti1_0_5 = Int(String(s3[0]))!
                
            }else{
                minuti2_0_9 = Int(String(s3[0]))!
                minuti1_0_5 = 0
            }
            secondi = secondo2
            secondi0_5 = secondo3
            
            _ = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                impactFeedbackgenerator.prepare()
                impactFeedbackgenerator.impactOccurred()
                self.playSound()
                if(self.secondi < 9)
                {
                    self.secondi+=1
                    self.indice = 0
                    self.secondi2.reloadData()
                    
                }else{
                    self.indice = 0
                    self.secondi = 0
                    self.indice1 = 0
                    self.secondi0_5+=1
                    self.secondi1.reloadData()
                    self.secondi2.reloadData()
                }
                if(self.secondi0_5 == 6)
                {
                    self.indice2 = 0
                    self.minuti2_0_9+=1
                    self.minuti2.reloadData()
                    self.secondi0_5 = 0
                }
                if(self.minuti2_0_9 == 10)
                {
                    
                    self.minuti2_0_9 = 0
                    self.indice3 = 0
                    self.minuti1_0_5+=1
                    
                    self.minuti2.reloadData()
                    self.minuti1.reloadData()
                }
                if(self.minuti1_0_5 > 5)
                {
                    self.minuti2_0_9 = 0
                    self.minuti1_0_5 = 0
                    self.ore_0_9+=1
                    self.indice4 = 0
                    self.Ore2.reloadData()
                    
                }
                if(self.ore_0_9 == 10)
                {
                    self.ore_0_1+=1
                    self.ore_0_9 = 0
                    self.indice4 = 0
                    self.indice5 = 0
                    self.ore1.reloadData()
                    self.Ore2.reloadData()
                }
                
                if self.ore_0_1 == 2 && self.ore_0_9 == 3 && self.secondi == 9 && self.secondi0_5 == 5 && self.minuti1_0_5 == 5, self.minuti2_0_9 == 9
                {
                    self.ore_0_1 = 0
                    self.ore_0_9 = 0
                    self.secondi0_5 = 0
                    self.secondi = 0
                    self.minuti1_0_5 = 0
                    self.minuti2_0_9 = 0
                    self.ore1.reloadData()
                    self.Ore2.reloadData()
                    self.minuti1.reloadData()
                    self.minuti2.reloadData()
                    self.secondi1.reloadData()
                    self.secondi2.reloadData()
                }
                
            }
            
        }
        func cambia ()
        {
            
        }
        override var prefersStatusBarHidden: Bool {
            return true
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if(collectionView == secondi1)
            {
                return 15
            }else if (collectionView == secondi2){
                return 15
            }else if collectionView == minuti2{
                return 15
            }else if collectionView == minuti1{
                return 15
            }else{
                return 1000
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            if(collectionView == secondi2)
            {
                var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! cellController
                
                if(self.secondi == 0)
                {
                    switch self.numero0[indice] {
                    case true:
                        cell.positiveValue()
                        break;
                        
                    case false:
                        cell.negativeValue()
                        break;
                        
                    default:
                        break;
                    }
                }else if(self.secondi == 1)
                {
                    switch self.numero1[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi == 2)
                {
                    switch self.numero2[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                        break;
                    default:
                        break;
                    }
                }else if(self.secondi == 3)
                {
                    switch self.numero3[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi == 4)
                {
                    switch self.numero4[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi == 5)
                {
                    switch self.numero5[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi == 6)
                {
                    switch self.numero6[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi == 7)
                {
                    switch self.numero7[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi == 8)
                {
                    switch self.numero8[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi == 9)
                {
                    switch self.numero9[indice] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }
                
                if(indice < 15)
                {
                    indice+=1
                }else{
                    indice = 0
                }
                
                return cell
            }else if collectionView == secondi1 {
                var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cella", for: indexPath) as! cellController
                
                if(self.secondi0_5 == 0)
                {
                    switch self.numero0[indice1] {
                    case true:
                        cell.positiveValue()
                        break;
                        
                    case false:
                        cell.negativeValue()
                        break;
                        
                    default:
                        break;
                    }
                }else if(self.secondi0_5 == 1)
                {
                    switch self.numero1[indice1] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi0_5 == 2)
                {
                    switch self.numero2[indice1] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                        break;
                    default:
                        break;
                    }
                }else if(self.secondi0_5 == 3)
                {
                    switch self.numero3[indice1] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi0_5 == 4)
                {
                    switch self.numero4[indice1] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.secondi0_5 == 5)
                {
                    switch self.numero5[indice1] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }
                
                
                if(indice1 < 15)
                {
                    indice1+=1
                }else{
                    indice1 = 0
                }
                return cell
            }else if collectionView == minuti2{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellC", for: indexPath) as! cellController
                if(self.minuti2_0_9 == 0)
                {
                    switch self.numero0[indice2] {
                    case true:
                        cell.positiveValue()
                        break;
                        
                    case false:
                        cell.negativeValue()
                        break;
                        
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 1)
                {
                    switch self.numero1[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 2)
                {
                    switch self.numero2[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                        break;
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 3)
                {
                    switch self.numero3[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 4)
                {
                    switch self.numero4[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 5)
                {
                    switch self.numero5[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 6)
                {
                    switch self.numero6[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 7)
                {
                    switch self.numero7[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 8)
                {
                    switch self.numero8[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti2_0_9 == 9)
                {
                    switch self.numero9[indice2] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }
                
                if(indice2 < 15)
                {
                    indice2+=1
                }else{
                    indice2 = 0
                }
                return cell
            }else if collectionView == minuti1{
                var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellaA", for: indexPath) as! cellController
                
                if(self.minuti1_0_5 == 0)
                {
                    switch self.numero0[indice3] {
                    case true:
                        cell.positiveValue()
                        break;
                        
                    case false:
                        cell.negativeValue()
                        break;
                        
                    default:
                        break;
                    }
                }else if(self.minuti1_0_5 == 1)
                {
                    switch self.numero1[indice3] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti1_0_5 == 2)
                {
                    switch self.numero2[indice3] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                        break;
                    default:
                        break;
                    }
                }else if(self.minuti1_0_5 == 3)
                {
                    switch self.numero3[indice3] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti1_0_5 == 4)
                {
                    switch self.numero4[indice3] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.minuti1_0_5 == 5)
                {
                    switch self.numero5[indice3] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }
                
                
                if(indice3 < 15)
                {
                    indice3+=1
                }else{
                    indice3 = 0
                }
                
                return cell
            }
            else if collectionView == Ore2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellM", for: indexPath) as! cellController
                if(self.ore_0_9 == 0)
                {
                    switch self.numero0[indice4] {
                    case true:
                        cell.positiveValue()
                        break;
                        
                    case false:
                        cell.negativeValue()
                        break;
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 1)
                {
                    switch self.numero1[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 2)
                {
                    switch self.numero2[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                        break;
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 3)
                {
                    switch self.numero3[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 4)
                {
                    switch self.numero4[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 5)
                {
                    switch self.numero5[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 6)
                {
                    switch self.numero6[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 7)
                {
                    switch self.numero7[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 8)
                {
                    switch self.numero8[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_9 == 9)
                {
                    switch self.numero9[indice4] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }
                
                if(indice4 < 15)
                {
                    indice4+=1
                }else{
                    indice4 = 0
                }
                return cell
                
                
                
            }
            else if collectionView == ore1{
                var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellV", for: indexPath) as! cellController
                
                if(self.ore_0_1 == 0)
                {
                    switch self.numero0[indice5] {
                    case true:
                        cell.positiveValue()
                        break;
                        
                    case false:
                        cell.negativeValue()
                        break;
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_1 == 1)
                {
                    switch self.numero1[indice5] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }else if(self.ore_0_1 == 2)
                {
                    switch self.numero1[indice5] {
                    case true:
                        cell.positiveValue()
                        
                    case false:
                        cell.negativeValue()
                        
                    default:
                        break;
                    }
                }
                
                
                
                if(indice5 < 15)
                {
                    indice5+=1
                }else{
                    indice5 = 0
                }
                
                return cell
            }
            else 
            {
                
                var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! cellController
                cell.negativeValue()
                return cell
            }
        }
        var i : Int{
            get{
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                return appDelegate.i
                
                
            }
            set{
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.i = newValue
                
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
           
           
            if(i == 6)
            {
                i = 0
                collectionView1.reloadData()
                ore1.reloadData()
                Ore2.reloadData()
                minuti2.reloadData()
                minuti1.reloadData()
                secondi1.reloadData()
                secondi2.reloadData()
                indice5=0
                indice4 = 0
                indice3 = 0
                indice2 = 0
                indice1 = 0
                indice = 0
            }else{
                i+=1
               
                ore1.reloadData()
                Ore2.reloadData()
                minuti2.reloadData()
                minuti1.reloadData()
                secondi1.reloadData()
                secondi2.reloadData()
                indice5=0
                indice4 = 0
                indice3 = 0
                indice2 = 0
                indice1 = 0
                indice = 0
                collectionView1.reloadData()
                
            }
            
        }
        func playSound() {
            let url = Bundle.main.url(forResource: "click.low", withExtension: "wav")!
            
            do {
                player = try AVAudioPlayer(contentsOf: url)
                guard let player = player else { return }
                
                player.prepareToPlay()
                player.play()
                
            } catch let error as NSError {
                print(error.description)
            }
        }
    }
