import UIKit


public extension UIView{ //Estenxde l'intera classe UILabel con funzioni artificiali inserite dal programmatore. Ad ogni UiLabel
    //troverai queste funzioni e non sono a STRING1.

  /*
  Fade in a view with a duration

  - parameter duration: custom animation duration
  */
    func fadeI(duration: TimeInterval = 1.0) { // duration1 nome del parametro, duration2 ereditarietà con TimeInterval. Dura due secondi.
        UIView.animate(withDuration: duration, animations: {
        self.alpha = 1.0
            
    })
  }

  /*
  Fade out a view with a duration

  - parameter duration: custom animation duration
  */
    func fadeOu(duration: TimeInterval = 1.0) {
        UIView
            .animate(withDuration: duration, animations: {
        self.alpha = 0.0
                
    })
  }

}

class LSViewController: UIViewController {
    @IBOutlet weak var img: UIImageView!
    override func viewDidLoad() {
      
        super.viewDidLoad()
//       self.view.fadeI()
        _ = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: {
                    timer in
        self.view.fadeI()
        })
        _ = Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false, block: {
            timer in
            self.performSegue(withIdentifier: "Vai", sender: self)
self.view.fadeOu()

        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
